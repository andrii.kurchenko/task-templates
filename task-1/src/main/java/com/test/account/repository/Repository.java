package com.test.account.repository;

import java.util.Map;
import java.util.Set;

public interface Repository<T> {

    int save(T entity);

    T get(int entityId);

    Map<Integer, T> getAll();

    void delete(int entityId);

    void deleteAll(Set<Integer> entityIds);
}
