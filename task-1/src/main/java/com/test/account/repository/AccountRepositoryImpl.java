package com.test.account.repository;

import com.test.account.common.IncrementalIdGenerator;
import com.test.account.entity.Account;
import com.test.account.util.RepositoryUtil;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class AccountRepositoryImpl implements Repository<Account> {

    private static final String ACCOUNT_STORAGE_FILE_NAME = "accounts.txt";

    @Override
    public int save(Account account) {
        Map<Integer, Account> accounts = RepositoryUtil.readItems(ACCOUNT_STORAGE_FILE_NAME, Account.class);

        int accountId = IncrementalIdGenerator.incrementAndGetAccountId();
        account.setId(accountId);

        accounts.put(accountId, account);

        RepositoryUtil.saveItems(accounts, ACCOUNT_STORAGE_FILE_NAME);

        return accountId;
    }

    @Override
    public Account get(int entityId) {
        Map<Integer, Account> accounts = RepositoryUtil.readItems(ACCOUNT_STORAGE_FILE_NAME, Account.class);

        return accounts.get(entityId);
    }

    @Override
    public Map<Integer, Account> getAll() {
        return RepositoryUtil.readItems(ACCOUNT_STORAGE_FILE_NAME, Account.class);
    }

    @Override
    public void delete(int accountId) {
        Map<Integer, Account> accounts = RepositoryUtil.readItems(ACCOUNT_STORAGE_FILE_NAME, Account.class);

        Account account = accounts.get(accountId);

        accounts.remove(accountId, account);

        RepositoryUtil.saveItems(accounts, ACCOUNT_STORAGE_FILE_NAME);
    }

    @Override
    public void deleteAll(Set<Integer> entityIds) {
        Map<Integer, Account> accounts = RepositoryUtil.readItems(ACCOUNT_STORAGE_FILE_NAME, Account.class);

        Map<Integer, Account> notDeletedAccounts = accounts.entrySet()
            .stream()
            .filter(e -> !entityIds.contains(e.getKey()))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        RepositoryUtil.saveItems(notDeletedAccounts, ACCOUNT_STORAGE_FILE_NAME);
    }
}
