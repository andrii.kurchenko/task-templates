package com.test.account.repository;

import com.test.account.common.IncrementalIdGenerator;
import com.test.account.entity.Group;
import com.test.account.util.RepositoryUtil;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class GroupRepositoryImpl implements Repository<Group> {

    private static final String GROUP_STORAGE_FILE_NAME = "groups.txt";

    @Override
    public int save(Group group) {
        Map<Integer, Group> groups = RepositoryUtil.readItems(GROUP_STORAGE_FILE_NAME, Group.class);

        int groupId = IncrementalIdGenerator.incrementAndGetGroupId();
        group.setId(groupId);

        groups.put(groupId, group);

        RepositoryUtil.saveItems(groups, GROUP_STORAGE_FILE_NAME);

        return groupId;
    }

    @Override
    public Group get(int entityId) {
        Map<Integer, Group> groups = RepositoryUtil.readItems(GROUP_STORAGE_FILE_NAME, Group.class);

        return groups.get(entityId);
    }

    @Override
    public Map<Integer, Group> getAll() {
        return RepositoryUtil.readItems(GROUP_STORAGE_FILE_NAME, Group.class);
    }

    @Override
    public void delete(int groupId) {
        Map<Integer, Group> groups = RepositoryUtil.readItems(GROUP_STORAGE_FILE_NAME, Group.class);

        Group group = groups.get(groupId);

        groups.remove(groupId, group);

        RepositoryUtil.saveItems(groups, GROUP_STORAGE_FILE_NAME);
    }

    @Override
    public void deleteAll(Set<Integer> groupIds) {
        Map<Integer, Group> groups = RepositoryUtil.readItems(GROUP_STORAGE_FILE_NAME, Group.class);

        Map<Integer, Group> notDeletedGroups = groups.entrySet()
            .stream()
            .filter(e -> !groupIds.contains(e.getKey()))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        RepositoryUtil.saveItems(notDeletedGroups, GROUP_STORAGE_FILE_NAME);
    }
}
