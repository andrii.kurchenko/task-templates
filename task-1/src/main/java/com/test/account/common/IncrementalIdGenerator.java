package com.test.account.common;

public class IncrementalIdGenerator {

    private static int currentAccountId = 0;
    private static int currentGroupId = 0;

    public static int incrementAndGetAccountId() {
        return ++currentAccountId;
    }

    public static int incrementAndGetGroupId() {
        return ++currentGroupId;
    }
}
