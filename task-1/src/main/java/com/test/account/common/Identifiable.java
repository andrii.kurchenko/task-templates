package com.test.account.common;

public interface Identifiable {

    int getId();
}
