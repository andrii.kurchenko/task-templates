package com.test.account.util;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public class RepositoryUtil {

    private static final String RESOURCE_DIR_NAME = "resources";

    public static <T> void saveItems(Map<Integer, T> itemsCollection, String storageName) {
        try (ObjectOutputStream outputStream = new ObjectOutputStream(Files.newOutputStream(getPath(storageName)))) {
            outputStream.writeObject(itemsCollection);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static Path getPath(String storageName) {
        return Path.of(RESOURCE_DIR_NAME, storageName);
    }

    @SuppressWarnings("unchecked")
    public static <T> Map<Integer, T> readItems(String storageName, Class<T> expectedType) {
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(buildResourcePath(storageName)))) {
            return (Map<Integer, T>) inputStream.readObject();
        } catch (EOFException e) {
            return new HashMap<>();
        } catch (ClassNotFoundException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String buildResourcePath(String storageName) {
        return RESOURCE_DIR_NAME + File.separator + storageName;
    }
}
