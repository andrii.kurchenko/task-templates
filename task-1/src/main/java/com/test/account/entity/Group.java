package com.test.account.entity;

import com.test.account.common.Identifiable;

import java.io.Serializable;

public class Group implements Identifiable, Serializable {

    private int id;
    private String name;

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("Group: [id=%d], [name = %s]", id, name);
    }
}
