package com.test.account.entity;

import com.test.account.common.Identifiable;

import java.io.Serializable;

public class Account implements Identifiable, Serializable {

    public int id;
    private String firstName;
    private String lastName;
    private Integer age;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return String.format(
            "Account: [id=%d], [firstName = %s], [lastName=%s], [age=%d]", id, firstName, lastName, age
        );
    }
}
