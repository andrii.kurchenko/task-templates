package com.test.account.service;

import com.test.account.entity.Group;
import com.test.account.repository.Repository;

import java.util.Map;
import java.util.Set;

public class GroupServiceImpl implements Service<Group> {

    private final Repository<Group> groupRepository;

    public GroupServiceImpl(Repository<Group> groupRepository) {
        this.groupRepository = groupRepository;
    }

    @Override
    public int save(Group group) {
        return groupRepository.save(group);
    }

    @Override
    public Group get(int groupId) {
        return groupRepository.get(groupId);
    }

    @Override
    public Map<Integer, Group> getAll() {
        return groupRepository.getAll();
    }

    @Override
    public void delete(int groupId) {
        groupRepository.delete(groupId);
    }

    @Override
    public void deleteAll(Set<Integer> groupIds) {
        groupRepository.deleteAll(groupIds);
    }
}
