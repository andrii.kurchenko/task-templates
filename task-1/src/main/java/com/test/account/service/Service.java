package com.test.account.service;

import com.test.account.common.Identifiable;

import java.util.Map;
import java.util.Set;

public interface Service<T extends Identifiable> {

    int save(T entity);

    T get(int entityId);

    Map<Integer, T> getAll();

    void delete(int entityId);

    void deleteAll(Set<Integer> entityIds);
}
