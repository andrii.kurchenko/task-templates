package com.test.account.service;

import com.test.account.entity.Account;
import com.test.account.repository.Repository;

import java.util.Map;
import java.util.Set;

public class AccountServiceImpl implements Service<Account> {

    private final Repository<Account> accountRepository;

    public AccountServiceImpl(Repository<Account> accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public int save(Account account) {
        return accountRepository.save(account);
    }

    @Override
    public Account get(int accountId) {
        return accountRepository.get(accountId);
    }

    @Override
    public Map<Integer, Account> getAll() {
        return accountRepository.getAll();
    }

    @Override
    public void delete(int accountId) {
        accountRepository.delete(accountId);
    }

    @Override
    public void deleteAll(Set<Integer> accountIds) {
        accountRepository.deleteAll(accountIds);
    }
}
