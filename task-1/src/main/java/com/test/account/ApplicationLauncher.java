package com.test.account;

import com.test.account.entity.Account;
import com.test.account.entity.Group;
import com.test.account.repository.AccountRepositoryImpl;
import com.test.account.repository.GroupRepositoryImpl;
import com.test.account.repository.Repository;
import com.test.account.service.AccountServiceImpl;
import com.test.account.service.GroupServiceImpl;
import com.test.account.service.Service;

import java.util.Set;

public class ApplicationLauncher {

    public static void main(String[] args) {
        interactWithAccounts();
        interactWithGroups();
    }

    private static void interactWithAccounts() {
        Repository<Account> accountRepository = new AccountRepositoryImpl();

        Service<Account> accountService = new AccountServiceImpl(accountRepository);

        Account firstAccount = new Account();
        firstAccount.setFirstName("Andrii");
        firstAccount.setLastName("Kurchenko");
        firstAccount.setAge(23);

        int firstAccountId = accountService.save(firstAccount);

        Account secondAccount = new Account();
        secondAccount.setFirstName("Tom");
        secondAccount.setLastName("Lite");
        secondAccount.setAge(24);

        int secondAccountId = accountService.save(firstAccount);

        System.out.println(accountService.getAll());

        accountService.delete(firstAccountId);

        System.out.println(accountService.get(secondAccountId));

        accountService.delete(secondAccountId);

        System.out.println(accountService.getAll());
    }

    private static void interactWithGroups() {
        Repository<Group> groupRepository = new GroupRepositoryImpl();

        Service<Group> groupService = new GroupServiceImpl(groupRepository);

        Group firstGroup = new Group();
        firstGroup.setName("Team-A");

        int firstGroupId = groupService.save(firstGroup);

        Group secondGroup = new Group();
        secondGroup.setName("Team-B");

        int secondGroupId = groupService.save(secondGroup);

        Group thirdGroup = new Group();
        thirdGroup.setName("Team-C");

        int thirdGroupId = groupService.save(thirdGroup);

        System.out.println(groupService.getAll());

        groupService.deleteAll(Set.of(firstGroupId, secondGroupId));

        System.out.println(groupService.getAll());

        groupService.delete(thirdGroupId);

        System.out.println(groupService.getAll());
    }
}
