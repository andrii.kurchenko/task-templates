package com.spdu.task3.entity;

import java.time.LocalDateTime;

public abstract class Expense {

    private LocalDateTime createdAt;
    private int lendedBy;
    private ExpenseType type;

    private Currency currency;
    private Double amount;
}