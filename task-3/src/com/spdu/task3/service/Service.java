package com.spdu.task3.service;

import com.spdu.task3.common.Identifiable;

import java.util.Collection;

public interface Service<T extends Identifiable> {

    T save(T entity);

    void delete(T entity);

    Collection<T> get(Collection<Integer> ids);

    Object get(Integer id);
}