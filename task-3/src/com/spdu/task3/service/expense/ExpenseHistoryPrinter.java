package com.spdu.task3.service.expense;

import com.spdu.task3.common.Sort;
import com.spdu.task3.entity.Group;

import java.time.LocalDateTime;

public interface ExpenseHistoryPrinter {

    void printAccountExpenseHistory(int accountId, Sort sort);

    void printGroupExpenseHistory(Group group, Sort sort);

    void printAccountExpenseHistoryGroupByCurrencies(int accountId, Sort sort);

    void printGroupExpenseHistoryGroupByCurrencies(Group group, Sort sort);

    void printAccountExpenseHistoryGroupByCurrencies(int accountId, Sort sort, LocalDateTime afterDate);

    void printGroupExpenseHistoryGroupByCurrencies(Group group, Sort sort, LocalDateTime afterDate);
}