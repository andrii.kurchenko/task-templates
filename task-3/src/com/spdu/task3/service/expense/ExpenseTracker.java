package com.spdu.task3.service.expense;

import com.spdu.task3.entity.Expense;

import java.util.Map;
import java.util.Set;

public interface ExpenseTracker {

    void trackExpense(Expense expense);

    Set<Expense> getExpensesByGroup(int groupId);

    Set<Expense> getExpensesByUser(int userId);

    Set<Expense> getExpensesForUserByLender(int userId, int lenderId);

    Set<Expense> getExpensesForGroupByLender(int userId, int lenderId, int groupId);

    Map<Integer, Set<Expense>> getExpensesByGroups(Set<Integer> groupIds);

    Map<Integer, Set<Expense>> getExpensesByUsers(Set<Integer> userIds);
}