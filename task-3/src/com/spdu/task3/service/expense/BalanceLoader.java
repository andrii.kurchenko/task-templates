package com.spdu.task3.service.expense;

import com.spdu.task3.entity.Account;
import com.spdu.task3.entity.Balance;
import com.spdu.task3.entity.Currency;
import com.spdu.task3.entity.Group;

import java.time.LocalDateTime;
import java.util.Map;

public interface BalanceLoader {

    Balance getAccountBalance(Group group, Account account);

    Balance getAccountBalance(Account account);

    Balance getAccountBalance(Group group, Account account, LocalDateTime afterDate);

    Balance getAccountBalance(Account account, LocalDateTime afterDate);

    Map<Currency, Balance> getAccountBalanceByCurrencies(Group group, Account account);

    Map<Currency, Balance> getAccountBalanceByCurrencies(Account account);
}