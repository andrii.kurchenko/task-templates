package com.spdu.task3.common.validator;

import com.spdu.task3.common.Identifiable;

public interface Validatable<T extends Identifiable> {

    boolean validate(T t);
}
