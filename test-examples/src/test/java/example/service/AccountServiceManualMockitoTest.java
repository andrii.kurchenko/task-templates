package example.service;

import com.spd.example.entity.Account;
import com.spd.example.exception.AccountNotFoundException;
import com.spd.example.exception.AccountValidationException;
import com.spd.example.repository.AccountRepository;
import com.spd.example.service.AccountService;
import com.spd.example.validator.AccountValidationPredicate;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class AccountServiceManualMockitoTest {

    @Test
    public void save_shouldThrowExceptionWhenAccountNotValid() {
        Account account = new Account();

        AccountRepository accountRepository = mock(AccountRepository.class);

        AccountValidationPredicate accountValidationPredicate = mock(AccountValidationPredicate.class);
        when(accountValidationPredicate.test(account)).thenReturn(false);

        AccountService accountService = new AccountService(accountRepository, accountValidationPredicate);

        assertThrows(
            AccountValidationException.class,
            () -> accountService.save(account),
            "Account not valid"
        );

        verifyNoMoreInteractions(accountRepository);
    }

    @Test
    public void save_shouldCorrectlySaveAccount_andReturnAccountWithNewId() {
        int generatedAccountId = 1;

        Account account = new Account();
        account.setFirstName("Name");
        account.setLastName("Surname");
        account.setPhoneNumber("1234");

        AccountRepository accountRepository = mock(AccountRepository.class);
        when(accountRepository.save(account)).thenReturn(generatedAccountId);

        AccountValidationPredicate accountValidationPredicate = mock(AccountValidationPredicate.class);
        when(accountValidationPredicate.test(account)).thenReturn(true);

        AccountService accountService = new AccountService(accountRepository, accountValidationPredicate);

        Account savedAccount = accountService.save(account);

        assertEquals(generatedAccountId, savedAccount.getId());
    }

    @Test
    public void delete_shouldThrowException_whenAccountNotExist() {
        int accountId = 1;

        AccountRepository accountRepository = mock(AccountRepository.class);

        AccountValidationPredicate accountValidationPredicate = mock(AccountValidationPredicate.class);

        AccountService accountService = new AccountService(accountRepository, accountValidationPredicate);

        assertThrows(
            AccountNotFoundException.class,
            () -> accountService.delete(accountId),
            "Account with id = '1' not found"
        );

        verify(accountRepository).find(accountId);
        verify(accountRepository, times(0)).delete(accountId);
    }

    @Test
    public void delete_shouldDeleteAccount_whenAccountPresent() {
        int accountId = 1;

        AccountRepository accountRepository = mock(AccountRepository.class);
        when(accountRepository.find(accountId)).thenReturn(new Account());

        AccountValidationPredicate accountValidationPredicate = mock(AccountValidationPredicate.class);

        AccountService accountService = new AccountService(accountRepository, accountValidationPredicate);

        accountService.delete(accountId);

        verify(accountRepository).delete(accountId);
    }
}
