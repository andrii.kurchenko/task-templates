package example.service;

import com.spd.example.entity.Account;
import com.spd.example.exception.AccountNotFoundException;
import com.spd.example.exception.AccountValidationException;
import com.spd.example.repository.AccountRepository;
import com.spd.example.service.AccountService;
import com.spd.example.validator.AccountValidationPredicate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AccountServiceAnnotationDrivenMockitoTest {

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private AccountValidationPredicate accountValidationPredicate;

    @InjectMocks
    private AccountService accountService;

    @Test
    public void save_shouldThrowExceptionWhenAccountNotValid() {
        Account account = new Account();

        when(accountValidationPredicate.test(account)).thenReturn(false);

        assertThrows(
            AccountValidationException.class,
            () -> accountService.save(account),
            "Account not valid"
        );

        verifyNoMoreInteractions(accountRepository);
    }

    @Test
    public void save_shouldCorrectlySaveAccount_andReturnAccountWithNewId() {
        int generatedAccountId = 1;

        Account account = new Account();
        account.setFirstName("Name");
        account.setLastName("Surname");
        account.setPhoneNumber("1234");

        when(accountRepository.save(account)).thenReturn(generatedAccountId);
        when(accountValidationPredicate.test(account)).thenReturn(true);

        Account savedAccount = accountService.save(account);

        assertEquals(generatedAccountId, savedAccount.getId());
    }

    @Test
    public void delete_shouldThrowException_whenAccountNotExist() {
        int accountId = 1;

        assertThrows(
            AccountNotFoundException.class,
            () -> accountService.delete(accountId),
            "Account with id = '1' not found"
        );

        verify(accountRepository).find(accountId);
        verify(accountRepository, times(0)).delete(accountId);
    }

    @Test
    public void delete_shouldDeleteAccount_whenAccountPresent() {
        int accountId = 1;

        when(accountRepository.find(accountId)).thenReturn(new Account());

        accountService.delete(accountId);

        verify(accountRepository).delete(accountId);
    }
}
