package example.validator;

import com.spd.example.entity.Account;
import com.spd.example.validator.AccountValidationPredicate;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AccountValidationPredicateTest {

    @Test
    public void test_shouldReturnFalse_whenAccountFirstNameIsNull_orFirstNameIsEmpty() {
        String emptyFirstName = "  ";

        Account firstAccount = new Account();
        firstAccount.setFirstName(null);

        Account secondAccount = new Account();
        secondAccount.setFirstName(emptyFirstName);

        AccountValidationPredicate accountValidationPredicate = new AccountValidationPredicate();

        assertFalse(accountValidationPredicate.test(firstAccount));
        assertFalse(accountValidationPredicate.test(secondAccount));
    }

    @Test
    public void test_shouldReturnFalse_whenAccountLastNameIsNull_orLastNameIsEmpty() {
        String firstName = "Name";
        String emptyLastName = "  ";

        Account firstAccount = new Account();
        firstAccount.setLastName(null);
        firstAccount.setFirstName(firstName);

        Account secondAccount = new Account();
        secondAccount.setLastName(emptyLastName);
        secondAccount.setFirstName(firstName);

        AccountValidationPredicate accountValidationPredicate = new AccountValidationPredicate();

        assertFalse(accountValidationPredicate.test(firstAccount));
        assertFalse(accountValidationPredicate.test(secondAccount));
    }

    @Test
    public void test_shouldReturnFalse_whenAccountPhoneNumberIsNull_orPhoneNumberIsEmpty() {
        String firstName = "Name";
        String lastName = "Surname";
        String emptyPhoneNumber = "   ";

        Account firstAccount = new Account();
        firstAccount.setLastName(lastName);
        firstAccount.setFirstName(firstName);
        firstAccount.setPhoneNumber(null);

        Account secondAccount = new Account();
        secondAccount.setLastName(lastName);
        secondAccount.setFirstName(firstName);
        secondAccount.setPhoneNumber(emptyPhoneNumber);

        AccountValidationPredicate accountValidationPredicate = new AccountValidationPredicate();

        assertFalse(accountValidationPredicate.test(firstAccount));
        assertFalse(accountValidationPredicate.test(secondAccount));
    }

    @Test
    public void test_shouldReturnTrue_whenAllAccountFieldsAreCorrect() {
        String firstName = "Name";
        String lastName = "Surname";
        String phoneNumber = "1234";

        Account account = new Account();
        account.setLastName(lastName);
        account.setFirstName(firstName);
        account.setPhoneNumber(phoneNumber);

        AccountValidationPredicate accountValidationPredicate = new AccountValidationPredicate();

        assertTrue(accountValidationPredicate.test(account));
    }
}
