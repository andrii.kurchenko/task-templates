package com.spd.example.service;

import com.spd.example.entity.Account;
import com.spd.example.exception.AccountNotFoundException;
import com.spd.example.exception.AccountValidationException;
import com.spd.example.repository.AccountRepository;
import com.spd.example.validator.AccountValidationPredicate;

public class AccountService {

    private final AccountRepository accountRepository;
    private final AccountValidationPredicate accountValidationPredicate;

    public AccountService(AccountRepository accountRepository,
                          AccountValidationPredicate accountValidationPredicate) {
        this.accountRepository = accountRepository;
        this.accountValidationPredicate = accountValidationPredicate;
    }

    public Account save(Account account) {
        if (!accountValidationPredicate.test(account)) {
            throw new AccountValidationException("Account not valid");
        }

        int generatedId = accountRepository.save(account);

        account.setId(generatedId);

        return account;
    }

    public void delete(int accountId) {
        Account account = accountRepository.find(accountId);

        if (account == null) {
            throw new  AccountNotFoundException(String.format("Account with id = '%d' not found", accountId));
        }

        accountRepository.delete(accountId);
    }
}
