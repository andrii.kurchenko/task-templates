package com.spd.example.validator;

import com.spd.example.entity.Account;

import java.util.function.Predicate;

public class AccountValidationPredicate implements Predicate<Account> {

    @Override
    public boolean test(Account account) {
        String firstName = account.getFirstName();
        if (firstName == null || firstName.isBlank()) {
            return false;
        }

        String lastName = account.getLastName();
        if (lastName == null || lastName.isBlank()) {
            return false;
        }

        String phoneNumber = account.getPhoneNumber();
        if (phoneNumber == null || phoneNumber.isBlank()) {
            return false;
        }

        return true;
    }
}
